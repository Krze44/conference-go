from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_weather(city):
    r = requests.get(f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid={OPEN_WEATHER_API_KEY}")

    data = json.loads(r.content)
    
    
    try:
        return {
        "temp": ((data["main"]["temp"] - 273.15) * 9/5 + 32) // 1,
        "description": data["weather"][0]["description"]
        }
    except(KeyError, IndexError):
        return {"weather": None}

def get_photo(city, state):
    header = {"Authorization": PEXELS_API_KEY}
    r = requests.get(f"https://api.pexels.com/v1/search?query={city},{state}&per_page=1", headers=header)
    
    data = json.loads(r.content)
    
    
    try:
        return {"picture_url": data["photos"][0]["src"]["small"]}
    except (KeyError, IndexError):
        return {"picture_url": None}